
const int baton_D = 5;
const int baton_C = 4;
const int baton_B = 3;
const int baton_A = 2;
const int jostik = 8;
const int x_axis = A1;
const int y_axis = A0;


void setup()
{
  pinMode(baton_D, INPUT);
  pinMode(baton_C, INPUT);
  pinMode(baton_B, INPUT);
  pinMode(baton_A, INPUT);
  pinMode(jostik, INPUT);
  pinMode(x_axis, INPUT);
  pinMode(y_axis, INPUT);
  Serial.begin(115200);
}

void loop()
{
  int baton_D_state = digitalRead(baton_D);
  int baton_C_state = digitalRead(baton_C);
  int baton_B_state = digitalRead(baton_B);
  int baton_A_state = digitalRead(baton_A);
  int jostik_state = digitalRead(baton_D);
  int x_axis_state = analogRead(x_axis);
  int y_axis_state = analogRead(y_axis);
  /*Serial.print("baton_A_state = ");
    Serial.println(baton_A_state);
    Serial.print("baton_B_state = ");
    Serial.println(baton_B_state);
    Serial.print("baton_C_state = ");
    Serial.println(baton_C_state);
    Serial.print("baton_D_state = ");
    Serial.println(baton_D_state);
    Serial.print("jostik_state = ");
    Serial.println(jostik_state);
    Serial.print("x_axis_state = ");
    Serial.println(x_axis_state);
    Serial.print("y_axis_state = ");
    Serial.println(y_axis_state);
    Serial.println("======================= ");
    delay (500);*/
  if (baton_A_state == 0) {
    Serial.write('A');
  }
  else if (baton_B_state == 0) {
    Serial.write('B');
  }
  else if (baton_C_state == 0) {
    Serial.write('C');
  }
  else if (baton_D_state == 0) {
    Serial.write('D');
  }
  else Serial.write('S');

  //Serial.write (x_axis_state);
  //delay(100);
}
