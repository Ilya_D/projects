#include <SoftwareSerial.h>

const int RX = 5;
const int TX = 4;
SoftwareSerial mySerial(RX,TX); // RX, TX

const int L_MOTOR_SPEED = 10;
const int R_MOTOR_SPEED = 11;
const int L_motor_1 = 6;
const int L_motor_2 = 7;
const int R_motor_1 = 8;
const int R_motor_2 = 9;

int L_SPEED = 115;
int R_SPEED = 200;
char val;

void forward (int t) {
  digitalWrite (R_motor_1, HIGH);
  digitalWrite (R_motor_2, LOW);
  digitalWrite (L_motor_1, HIGH);
  digitalWrite (L_motor_2, LOW);
  analogWrite (R_MOTOR_SPEED, R_SPEED);
  analogWrite (L_MOTOR_SPEED, L_SPEED);
  delay (t);
}
void backward (int t) {
  digitalWrite (R_motor_1, LOW);
  digitalWrite (R_motor_2, HIGH);
  digitalWrite (L_motor_1, LOW);
  digitalWrite (L_motor_2, HIGH);
  analogWrite (R_MOTOR_SPEED, R_SPEED);
  analogWrite (L_MOTOR_SPEED, L_SPEED);
  delay (t);
}
void right (int t) {
  digitalWrite (R_motor_1, LOW);
  digitalWrite (R_motor_2, HIGH);
  digitalWrite (L_motor_1, HIGH);
  digitalWrite (L_motor_2, LOW);
  analogWrite (R_MOTOR_SPEED, R_SPEED + 50);
  analogWrite (L_MOTOR_SPEED, L_SPEED + 50);
  delay (t);
}
void left (int t) {
  digitalWrite (R_motor_1, HIGH);
  digitalWrite (R_motor_2, LOW);
  digitalWrite (L_motor_1, LOW);
  digitalWrite (L_motor_2, HIGH);
  analogWrite (R_MOTOR_SPEED, R_SPEED + 50);
  analogWrite (L_MOTOR_SPEED, L_SPEED + 50);
  delay (t);
}
void Stope (int t) {
  digitalWrite (R_motor_1, LOW);
  digitalWrite (R_motor_2, LOW);
  digitalWrite (L_motor_1, LOW);
  digitalWrite (L_motor_2, LOW);
  analogWrite (R_MOTOR_SPEED, R_SPEED);
  analogWrite (L_MOTOR_SPEED, L_SPEED);
  delay (t);
}
void setup() {
  pinMode (R_motor_1, OUTPUT);
  pinMode (R_motor_2, OUTPUT);
  pinMode (L_motor_1, OUTPUT);
  pinMode (L_motor_2, OUTPUT);
  pinMode (L_MOTOR_SPEED, OUTPUT);
  pinMode (R_MOTOR_SPEED, OUTPUT);
  pinMode (RX, INPUT);
  pinMode (TX, OUTPUT);
  Serial.begin (9600);
  mySerial.begin(115200);
}

void loop() {/*
  if (mySerial.available()) {
    val = mySerial.read();
    Serial.println (val);
  }
  if (val == 'A') {
    forward (10);
  }
  if (val == 'B') {
    right (10);
  }
  if (val == 'C') {
    backward (10);
  }
  if (val == 'D') {
    left (10);
  }
  if (val == 'S') {
    Stope(10);
  }*/
  forward (1000);
  backward (1000);
}
