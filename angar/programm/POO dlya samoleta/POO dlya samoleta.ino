#include <AFMotor.h>

AF_DCMotor motor1(1);  // создаем объект motor №1
AF_DCMotor motor2(2);  // создаем объект motor №2
AF_DCMotor motor3(3);  // создаем объект motor №3
AF_DCMotor motor4(4);  // создаем объект motor №4

const int pump_1 = A0;
const int pump_2 = A1;
const int pump_3 = A2;
const int pump_4 = A3;

void setup() {
  Serial.begin(9600);  // устанавливаем скоросьб передачи по посл. порту 9600 бод
  Serial.println("Тест двигателя");
  pinMode(pump_1, OUTPUT);
  pinMode(pump_2, OUTPUT);
  pinMode(pump_3, OUTPUT);
  pinMode(pump_4, OUTPUT);
  motor1.setSpeed(130);  // устанавливаем скорость вращения 200/255
  motor2.setSpeed(85);
  motor3.setSpeed(90);
  motor4.setSpeed(75);
}

void loop() {

  // насосы
digitalWrite(pump_1, HIGH);
  delay(1000);
  digitalWrite(pump_1, LOW);
  delay(1000);
  digitalWrite(pump_2, HIGH);
  delay(1000);
  digitalWrite(pump_2, LOW);
  delay(1000);
  digitalWrite(pump_3, HIGH);
  delay(1000);
  digitalWrite(pump_3, LOW);
  delay(1000);
  digitalWrite(pump_4, HIGH);
  delay(1000);
  digitalWrite(pump_4, LOW);
  delay(1000);
  motor1.run(FORWARD);  // вращение вперед
  motor2.run(BACKWARD);
  
  delay(800);

  motor1.run(RELEASE);  // останавливаем вращение
  motor2.run(RELEASE);
  delay(1500);
  digitalWrite(pump_1, HIGH);
  digitalWrite(pump_2, HIGH);
  //движ.балки
  digitalWrite(pump_3, HIGH);
  digitalWrite(pump_4, HIGH);
  motor3.run(FORWARD);  // вращение вперед
  
  motor4.run(BACKWARD);
  delay(1800);
  motor3.run(RELEASE);  // останавливаем вращение
  motor4.run(RELEASE);
  delay(1000);
   digitalWrite(pump_1, LOW);
  digitalWrite(pump_2, LOW);
  digitalWrite(pump_3, LOW);
  digitalWrite(pump_4, LOW);
  motor1.run(BACKWARD);  // в другую сторону
  motor2.run(FORWARD);
  delay(790);
  motor1.run(RELEASE);  // останавливаем вращение
  motor2.run(RELEASE);
  delay(1000);
  digitalWrite(pump_1, HIGH);
  digitalWrite(pump_2, HIGH);
  digitalWrite(pump_3, HIGH);
  digitalWrite(pump_4, HIGH);
  motor3.run(FORWARD);  // вращение вперед
  motor4.run(BACKWARD);
  delay(1500);
  motor3.run(RELEASE);  // останавливаем вращение
  motor4.run(RELEASE);
  delay(750);
  digitalWrite(pump_1, LOW);
  digitalWrite(pump_2, LOW);
  digitalWrite(pump_3, LOW);
  digitalWrite(pump_4, LOW);
  motor1.run(FORWARD);  // вращение вперед
  motor2.run(BACKWARD);
  delay(800);
  motor1.run(RELEASE);  // останавливаем вращение
  motor2.run(RELEASE);
  delay(1000);
  digitalWrite(pump_1, HIGH);
  digitalWrite(pump_2, HIGH);
  digitalWrite(pump_3, HIGH);
  digitalWrite(pump_4, HIGH);
  motor3.run(FORWARD);  // вращение вперед
  motor4.run(BACKWARD);
  delay(2100);
  digitalWrite(pump_1, LOW);  //останавливаем насосы
  digitalWrite(pump_2, LOW);
  digitalWrite(pump_3, LOW);
  digitalWrite(pump_4, LOW);
  motor3.run(RELEASE);  // останавливаем вращение
  motor4.run(RELEASE);
  delay(800);
  motor1.run(BACKWARD);  // в другую сторону
  motor2.run(FORWARD);
  delay(780);
  motor1.run(RELEASE);  // останавливаем вращение
  motor2.run(RELEASE);
  delay(4500);
  motor3.run(BACKWARD);  // в другую сторону
  motor4.run(FORWARD);
  delay(4000);
  motor3.run(RELEASE);  // останавливаем вращение
  motor4.run(RELEASE);
  delay(3000);
  //  моторы на балке (мотор 1 и мотор 2)
}