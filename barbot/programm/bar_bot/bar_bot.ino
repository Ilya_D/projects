#include <AFMotor.h>
AF_DCMotor motor1(1);  // подключаем мотор к клеммникам M1
AF_DCMotor motor2(2);  // подключаем мотор к клеммникам M2
AF_DCMotor KARETKA(3); // подключаем мотор к клеммникам M3
AF_DCMotor motor4(4);  // подключаем мотор к клеммникам M4+

const int BUTTON_KARETKA = 10;

void karetka_on_start () {
  KARETKA.run(BACKWARD);  // задаем движение влево
  while (digitalRead (BUTTON_KARETKA)) {
    delay(1);
  }
  KARETKA.run(RELEASE);  // останавливаем мотор
}

void KARETKA_POS_1 (){
  KARETKA.run(FORWARD);  // задаем движение вправо к стакану №1
  delay (300);
  KARETKA.run(RELEASE);  // останавливаем мотор
}

void KARETKA_POS_2 (){
  KARETKA.run(FORWARD);  // задаем движение вправо к стакану №1
  delay (500);
  KARETKA.run(RELEASE);  // останавливаем мотор
}

void KARETKA_POS_3 (){
  KARETKA.run(FORWARD);  // задаем движение вправо к стакану №1
  delay (700);
  KARETKA.run(RELEASE);  // останавливаем мотор
}

void bottle_1_on (int t){
  motor1.run(FORWARD);   // включаем насос 1
  delay (t);
  motor1.run(RELEASE);   // останавливаем насос 1
}

void bottle_2_on (int t){
  motor2.run(FORWARD);   // включаем насос 1
  delay (t);
  motor2.run(RELEASE);   // останавливаем насос 1
}

void bottle_3_on (int t){
  motor4.run(FORWARD);   // включаем насос 1
  delay (t);
  motor4.run(RELEASE);   // останавливаем насос 1
}

void setup() {
  Serial.begin (9600);
  pinMode (BUTTON_KARETKA, INPUT_PULLUP);
  motor1.setSpeed(255);  // задаем максимальную скорость мотора
  motor1.run(RELEASE);   // останавливаем мотор
  motor2.setSpeed(255);  // задаем максимальную скорость мотора
  motor2.run(RELEASE);   // останавливаем мотор
  KARETKA.setSpeed(230); // задаем максимальную скорость мотора
  KARETKA.run(RELEASE);  // останавливаем мотор
  motor4.setSpeed(255);  // задаем максимальную скорость мотора
  motor4.run(RELEASE);   // останавливаем мотор
}

void loop() {
  Serial.print ("button state = ");
  Serial.println (digitalRead (BUTTON_KARETKA));
  karetka_on_start ();
  delay (3000);
  KARETKA_POS_1 ();
  bottle_1_on (2000);
  karetka_on_start ();
  delay (500);
  KARETKA_POS_2 ();
  bottle_2_on (1000);
  bottle_3_on (1000);
  karetka_on_start ();
  delay (500);
  KARETKA_POS_3 ();
  bottle_3_on (2000);
  bottle_1_on (2000);
}
