// --------------------- БИБЛИОТЕКИ ----------------------
#include <Servo.h>
#include "I2Cdev.h"
#include "MPU6050.h"
#include "Wire.h"

// --------------------- НАСТРОЙКИ ----------------------
const int buffersize_in_fly = 10;     // количество итераций калибровки во время полета
MPU6050 mpu(0x68);
int ax, ay, az, gx, gy, gz;
int mean_ax, mean_ay, mean_az, mean_gx, mean_gy, mean_gz, state = 0;
// моторы
Servo motor_1;
Servo motor_2;
Servo motor_3;
Servo motor_4;
const int mot_pin_4 = 9;
const int mot_pin_3 = 6;
const int mot_pin_2 = 5;
const int mot_pin_1 = 3;
/////////////////////////  ПЕРЕМЕННЫЕ  //////////////////////////////
int speed_hovering = 1400; //скорость зависания (1250 предположительно, около того)
int uping = 20; // переменная для подъема
int speed_correct = 30; // переменная для выравнивания
const int max_pwm = 1450;
const int min_pwm = 850;
const int X_ZERO = 175;
const int Y_ZERO = 70;
const int OFFSET = 20;
const int ZUM_ZUM = 11;  //зумер

/////////////////////////  FUNCTIONS   ////////////////////////////////

void overclocking() {         //функция раскрутки моторов
  for (int x = min_pwm; x < speed_hovering-50; x++) {
    motor_1.writeMicroseconds(x);
    motor_2.writeMicroseconds(x);
    motor_3.writeMicroseconds(x);
    motor_4.writeMicroseconds(x);
    delay(5);
  }
}
void STOP (int t) {           // функция остановки моторов
  motor_1.writeMicroseconds (0);
  motor_2.writeMicroseconds (0);
  motor_3.writeMicroseconds (0);
  motor_4.writeMicroseconds (0);
  delay (t);
}
int meansensors_in_fly() {    // получение средних данных с mpu6050 во время полета  a - отклонения. g - скорость отклонения.
  int accX, accY, accZ, gccX, gccY, gccZ;
  long i = 0, buff_ax = 0, buff_ay = 0, buff_az = 0, buff_gx = 0, buff_gy = 0, buff_gz = 0;
  while (i <= buffersize_in_fly) { // read raw accel/gyro measurements from device
    mpu.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
    if (i < buffersize_in_fly) { //First 100 measures are discarded
      buff_ax = buff_ax + ax;
      buff_ay = buff_ay + ay;
      buff_az = buff_az + az;
      buff_gx = buff_gx + gx;
      buff_gy = buff_gy + gy;
      buff_gz = buff_gz + gz;
      //Serial.println ("i am working");
    }
    if (i == buffersize_in_fly) {
      mean_ax = buff_ax / buffersize_in_fly;
      mean_ay = buff_ay / buffersize_in_fly;
      mean_az = buff_az / buffersize_in_fly;
      mean_gx = buff_gx / buffersize_in_fly;
      mean_gy = buff_gy / buffersize_in_fly;
      mean_gz = buff_gz / buffersize_in_fly;
      // перевод в систему СИ
    //  mean_ax = accX / 32768 * 2;
    //  mean_ay = accY / 32768 * 2;
    //  mean_az = accZ / 32768 * 2;
    //  mean_gx = gccX / 32768 * 250;
    //  mean_gy = gccY / 32768 * 250;
    //  mean_gz = gccZ / 32768 * 250;
     // Serial.println ("i am working");
    }
    i++;
  }
}

/////////////////////////  SETUP   ////////////////////////////////////
void setup() {
  Wire.begin();
  Serial.begin(115200);
  mpu.initialize();
  pinMode (ZUM_ZUM, OUTPUT);
  pinMode (mot_pin_1, OUTPUT);
  pinMode (mot_pin_2, OUTPUT);
  pinMode (mot_pin_3, OUTPUT);
  pinMode (mot_pin_4, OUTPUT);

  // инициализация моторов
  motor_1.attach (mot_pin_1);
  motor_2.attach (mot_pin_2);
  motor_3.attach (mot_pin_3);
  motor_4.attach (mot_pin_4);
  delay (1000);
  motor_1.writeMicroseconds (max_pwm);
  motor_2.writeMicroseconds (max_pwm);
  motor_3.writeMicroseconds (max_pwm);
  motor_4.writeMicroseconds (max_pwm);
  delay (2000);
  motor_1.writeMicroseconds (min_pwm);
  motor_2.writeMicroseconds (min_pwm);
  motor_3.writeMicroseconds (min_pwm);
  motor_4.writeMicroseconds (min_pwm);
  delay (4000);

  // предупредительные сигналы
  digitalWrite(ZUM_ZUM, HIGH); delay(100); digitalWrite(ZUM_ZUM, LOW); delay(500); // первый писк
  digitalWrite(ZUM_ZUM, HIGH); delay(100); digitalWrite(ZUM_ZUM, LOW); delay(500); // второй писк
  digitalWrite(ZUM_ZUM, HIGH); delay(100); digitalWrite(ZUM_ZUM, LOW); delay(500); // третий писк
  Serial.println("пошел разгон");
  //разгон моторов
  overclocking (); // разгон
  digitalWrite(ZUM_ZUM, HIGH); delay(500); digitalWrite(ZUM_ZUM, LOW); delay(500); // первый писк
  Serial.println("понеслась");
}

/////////////////////////  LOOP   ////////////////////////////////////
void loop() {
  int i = 0;
  while ( i < 10) {
    meansensors_in_fly();
    Serial.print("отклонение по X= ");
    Serial.print(mean_ax);
    Serial.print("         |");
    Serial.print("отклонение по Y= ");
    Serial.print(mean_ay);
    Serial.print("          |");
    Serial.print("отклонение по Z= ");
    Serial.print(mean_az);
    Serial.println("         |");
    Serial.print("скорость отклонения по X= ");
    Serial.print(mean_gx);
    Serial.print(" |");
    Serial.print("скорость отклонения по Y= ");
    Serial.print(mean_gy);
    Serial.print(" |");
    Serial.print("скорость отклонения по Z= ");
    Serial.print(mean_gz);
    Serial.println(" |");
    Serial.println("|-------------------------------------------------------------------------------------------|");
    //Serial.println (i);
    i++;
//delay (400);
    if (mean_ax > (X_ZERO + OFFSET)) {
      motor_1.writeMicroseconds (speed_hovering + speed_correct);
      motor_2.writeMicroseconds (speed_hovering + speed_correct);
      motor_3.writeMicroseconds (speed_hovering - speed_correct);
      motor_4.writeMicroseconds (speed_hovering - speed_correct);
      Serial.println ("я наклоняюсь вперед");
    }
    else if (mean_ax < (X_ZERO - OFFSET)) {
      motor_1.writeMicroseconds (speed_hovering - speed_correct);
      motor_2.writeMicroseconds (speed_hovering - speed_correct);
      motor_3.writeMicroseconds (speed_hovering + speed_correct);
      motor_4.writeMicroseconds (speed_hovering + speed_correct);
      Serial.println ("я наклоняюсь назад");
    }
    else if (mean_ay < (Y_ZERO - OFFSET) ) {
      motor_1.writeMicroseconds (speed_hovering + speed_correct);
      motor_2.writeMicroseconds (speed_hovering - speed_correct);
      motor_3.writeMicroseconds (speed_hovering + speed_correct);
      motor_4.writeMicroseconds (speed_hovering - speed_correct);
      Serial.println ("я наклоняюсь направо");
    }
    else if (mean_ay > (Y_ZERO + OFFSET)) {
      motor_1.writeMicroseconds (speed_hovering - speed_correct);
      motor_2.writeMicroseconds (speed_hovering + speed_correct);
      motor_3.writeMicroseconds (speed_hovering - speed_correct);
      motor_4.writeMicroseconds (speed_hovering + speed_correct);
      Serial.println ("наклоняюсь налево");
    }
    else  {
      motor_1.writeMicroseconds (speed_hovering );
      motor_2.writeMicroseconds (speed_hovering );
      motor_3.writeMicroseconds (speed_hovering );
      motor_4.writeMicroseconds (speed_hovering );
      Serial.println ("я в ровном положении");
    }
  }
  STOP (1);
  digitalWrite(ZUM_ZUM, HIGH); delay(100); digitalWrite(ZUM_ZUM, LOW); delay(500);
  Serial.println ("полет окончен");
  while (true);
}
