// --------------------- БИБЛИОТЕКИ ----------------------
#include <Servo.h>
#include "I2Cdev.h"
#include "MPU6050.h"
#include "Wire.h"

// --------------------- НАСТРОЙКИ ----------------------
const int ZUM_ZUM = 11;  //зумер
const int buffersize = 200;     // количество итераций калибровки 
const int buffersize_in_fly = 30;     // количество итераций калибровки во время полета
const int acel_deadzone = 8;  // точность калибровки акселерометра (по умолчанию 8)
const int gyro_deadzone = 2;   // точность калибровки гироскопа (по умолчанию 2)
MPU6050 mpu(0x68);
int16_t ax, ay, az, gx, gy, gz;
int mean_ax, mean_ay, mean_az, mean_gx, mean_gy, mean_gz, state = 0;
int ax_offset, ay_offset, az_offset, gx_offset, gy_offset, gz_offset;
// моторы
Servo motor_1;
Servo motor_2;
Servo motor_3;
Servo motor_4;
const int mot_pin_4 = 9;
const int mot_pin_3 = 6;
const int mot_pin_2 = 5;
const int mot_pin_1 = 3;
/////////////////////////  ПЕРЕМЕННЫЕ  //////////////////////////////
int speed_hovering = 1280; //скорость зависания (1250 предположительно, около того)
int uping = 20; // переменная для подъема
int speed_correct = 15; // переменная для выравнивания
const int max_pwm = 1400;
const int min_pwm = 850;

/////////////////////////  FUNCTIONS   ////////////////////////////////

void overclocking() {         //функция раскрутки моторов
  for (int x = min_pwm; x < speed_hovering-10; x++) {
    motor_1.writeMicroseconds(x);
    motor_2.writeMicroseconds(x);
    motor_3.writeMicroseconds(x);
    motor_4.writeMicroseconds(x);
    delay(5);
  }
}
void STOP (int t) {           // функция остановки моторов
  motor_1.writeMicroseconds (0);
  motor_2.writeMicroseconds (0);
  motor_3.writeMicroseconds (0);
  motor_4.writeMicroseconds (0);
  delay (t);
}
void meansensors() {          // получение средних данных с mpu6050 для калибровки mpu6050  a - отклонения. g - скорость отклонения.
  long i = 0, buff_ax = 0, buff_ay = 0, buff_az = 0, buff_gx = 0, buff_gy = 0, buff_gz = 0;
  while (i < (buffersize + 101)) { // read raw accel/gyro measurements from device
    mpu.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
    if (i > 100 && i <= (buffersize + 100)) { //First 100 measures are discarded
      buff_ax = buff_ax + ax;
      buff_ay = buff_ay + ay;
      buff_az = buff_az + az;
      buff_gx = buff_gx + gx;
      buff_gy = buff_gy + gy;
      buff_gz = buff_gz + gz;
    }
    if (i == (buffersize + 100)) {
      mean_ax = buff_ax / buffersize;
      mean_ay = buff_ay / buffersize;
      mean_az = buff_az / buffersize;
      mean_gx = buff_gx / buffersize;
      mean_gy = buff_gy / buffersize;
      mean_gz = buff_gz / buffersize;
    }
    i++;
    delay(2);
  }
}
int meansensors_in_fly() {    // получение средних данных с mpu6050 во время полета  a - отклонения. g - скорость отклонения.
  long i = 0, buff_ax = 0, buff_ay = 0, buff_az = 0, buff_gx = 0, buff_gy = 0, buff_gz = 0;
  while (i <= buffersize_in_fly) { // read raw accel/gyro measurements from device
    mpu.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
    if (i < buffersize_in_fly) { //First 100 measures are discarded
      buff_ax = buff_ax + ax;
      buff_ay = buff_ay + ay;
      buff_az = buff_az + az;
      buff_gx = buff_gx + gx;
      buff_gy = buff_gy + gy;
      buff_gz = buff_gz + gz;
    }
    if (i == buffersize_in_fly) {
      mean_ax = buff_ax / buffersize_in_fly;
      mean_ay = buff_ay / buffersize_in_fly;
      mean_az = buff_az / buffersize_in_fly;
      mean_gx = buff_gx / buffersize_in_fly;
      mean_gy = buff_gy / buffersize_in_fly;
      mean_gz = buff_gz / buffersize_in_fly;
    }
    i++;
  }
}
void calibration() {          // функция калибровки данных с mpu6050
  ax_offset = -mean_ax / 8;
  ay_offset = -mean_ay / 8;
  az_offset = (16384 - mean_az) / 8;
  gx_offset = -mean_gx / 4;
  gy_offset = -mean_gy / 4;
  gz_offset = -mean_gz / 4;
  while (1) {
    int ready = 0;
    mpu.setXAccelOffset(ax_offset);
    mpu.setYAccelOffset(ay_offset);
    mpu.setZAccelOffset(az_offset);
    mpu.setXGyroOffset(gx_offset);
    mpu.setYGyroOffset(gy_offset);
    mpu.setZGyroOffset(gz_offset);
    meansensors();
    Serial.println("...");
    if (abs(mean_ax) <= acel_deadzone) ready++;
    else ax_offset = ax_offset - mean_ax / acel_deadzone;
    if (abs(mean_ay) <= acel_deadzone) ready++;
    else ay_offset = ay_offset - mean_ay / acel_deadzone;
    if (abs(16384 - mean_az) <= acel_deadzone) ready++;
    else az_offset = az_offset + (16384 - mean_az) / acel_deadzone;
    if (abs(mean_gx) <= gyro_deadzone) ready++;
    else gx_offset = gx_offset - mean_gx / (gyro_deadzone + 1);
    if (abs(mean_gy) <= gyro_deadzone) ready++;
    else gy_offset = gy_offset - mean_gy / (gyro_deadzone + 1);
    if (abs(mean_gz) <= gyro_deadzone) ready++;
    else gz_offset = gz_offset - mean_gz / (gyro_deadzone + 1);
    if (ready == 6) break;
  }
}

/////////////////////////  SETUP   ////////////////////////////////////
void setup() {
  Wire.begin();
  Serial.begin(115200);
  mpu.initialize();
  pinMode (ZUM_ZUM, OUTPUT);
  pinMode (mot_pin_1, OUTPUT);
  pinMode (mot_pin_2, OUTPUT);
  pinMode (mot_pin_3, OUTPUT);
  pinMode (mot_pin_4, OUTPUT);
  // сбросить оффсеты
  mpu.setXAccelOffset(0);
  mpu.setYAccelOffset(0);
  mpu.setZAccelOffset(0);
  mpu.setXGyroOffset(0);
  mpu.setYGyroOffset(0);
  mpu.setZGyroOffset(0);

  // инициализация моторов
  motor_1.attach (mot_pin_1);
  motor_2.attach (mot_pin_2);
  motor_3.attach (mot_pin_3);
  motor_4.attach (mot_pin_4);
  delay (1000);
  motor_1.writeMicroseconds (max_pwm);
  motor_2.writeMicroseconds (max_pwm);
  motor_3.writeMicroseconds (max_pwm);
  motor_4.writeMicroseconds (max_pwm);
  delay (2000);
  motor_1.writeMicroseconds (min_pwm);
  motor_2.writeMicroseconds (min_pwm);
  motor_3.writeMicroseconds (min_pwm);
  motor_4.writeMicroseconds (min_pwm);
  delay (4000);

  // калибровка mpu6050
  if (state == 0) {
    meansensors();
    state++;
    delay(1000);
    Serial.println("первое чтение датчика завершено, начинается калибровка MPU6050");
  }
  if (state == 1) {
    calibration();
    state++;
    delay(1000);
    Serial.println("калибровка MPU6050 завершена");
  }
  if (state == 2) {
    meansensors();
    Serial.println("повторное чтение MPU6050 завершено");
  }


  // предупредительные сигналы
  digitalWrite(ZUM_ZUM, HIGH); delay(100); digitalWrite(ZUM_ZUM, LOW); delay(500); // первый писк
  digitalWrite(ZUM_ZUM, HIGH); delay(100); digitalWrite(ZUM_ZUM, LOW); delay(500); // второй писк
  digitalWrite(ZUM_ZUM, HIGH); delay(100); digitalWrite(ZUM_ZUM, LOW); delay(500); // третий писк
  Serial.println("пошел разгон");
  //разгон моторов
  overclocking (); // разгон
  digitalWrite(ZUM_ZUM, HIGH); delay(200); digitalWrite(ZUM_ZUM, LOW); delay(500); // первый писк
  Serial.println("понеслась");
}

/////////////////////////  LOOP   ////////////////////////////////////
void loop() {
  int i = 0;
  while ( i < 20) {
    meansensors_in_fly();
    Serial.print("отклонение по X= ");
    Serial.print(mean_ax);
    Serial.print("         |");
    Serial.print("отклонение по Y= ");
    Serial.print(mean_ay);
    Serial.print("          |");
    Serial.print("отклонение по Z= ");
    Serial.print(mean_az);
    Serial.println("         |");
    Serial.print("скорость отклонения по X= ");
    Serial.print(mean_gx);
    Serial.print(" |");
    Serial.print("скорость отклонения по Y= ");
    Serial.print(mean_gy);
    Serial.print(" |");
    Serial.print("скорость отклонения по Z= ");
    Serial.print(mean_gz);
    Serial.println(" |");
    Serial.println("|-------------------------------------------------------------------------------------------|");
    //Serial.println (i);
    i++;

    if (mean_ax > 10 ) {
      motor_1.writeMicroseconds (speed_hovering + speed_correct);
      motor_2.writeMicroseconds (speed_hovering + speed_correct);
      motor_3.writeMicroseconds (speed_hovering - speed_correct);
      motor_4.writeMicroseconds (speed_hovering - speed_correct);
      Serial.println ("я наклоняюсь вперед");
    }
    else if (mean_ax < -10 ) {
      motor_1.writeMicroseconds (speed_hovering - speed_correct);
      motor_2.writeMicroseconds (speed_hovering - speed_correct);
      motor_3.writeMicroseconds (speed_hovering + speed_correct);
      motor_4.writeMicroseconds (speed_hovering + speed_correct);
      Serial.println ("я наклоняюсь назад");
    }
    else if (mean_ay < -10 ) {
      motor_1.writeMicroseconds (speed_hovering + speed_correct);
      motor_2.writeMicroseconds (speed_hovering - speed_correct);
      motor_3.writeMicroseconds (speed_hovering + speed_correct);
      motor_4.writeMicroseconds (speed_hovering - speed_correct);
      Serial.println ("я наклоняюсь направо");
    }
    else if (mean_ay > 10 ) {
      motor_1.writeMicroseconds (speed_hovering - speed_correct);
      motor_2.writeMicroseconds (speed_hovering + speed_correct);
      motor_3.writeMicroseconds (speed_hovering - speed_correct);
      motor_4.writeMicroseconds (speed_hovering + speed_correct);
      Serial.println ("наклоняюсь налево");
    }
    else  {
      motor_1.writeMicroseconds (speed_hovering );
      motor_2.writeMicroseconds (speed_hovering );
      motor_3.writeMicroseconds (speed_hovering );
      motor_4.writeMicroseconds (speed_hovering );
      Serial.println ("я в ровном положении");
    }
  }
  STOP (1);
  digitalWrite(ZUM_ZUM, HIGH); delay(100); digitalWrite(ZUM_ZUM, LOW); delay(500);
  Serial.println ("полет окончен");
  while (true);
}
