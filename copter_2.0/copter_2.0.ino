#include <Servo.h>
Servo motor_1;
Servo motor_2;
Servo motor_3;
Servo motor_4;
const int mot_pin_4 = 9;
const int mot_pin_3 = 6;
const int mot_pin_2 = 5;
const int mot_pin_1 = 3;
const int max_pwm = 1700;
const int min_pwm = 850;

void STOP (int t)
{
  motor_1.writeMicroseconds (0);
  motor_2.writeMicroseconds (0);
  motor_3.writeMicroseconds (0);
  motor_4.writeMicroseconds (0);
  delay (t);
}
void setup()
{
  Serial.begin (9600);
  motor_1.attach (mot_pin_1);
  motor_2.attach (mot_pin_2);
  motor_3.attach (mot_pin_3);
  motor_4.attach (mot_pin_4);
  //delay (1000);
  motor_1.writeMicroseconds (max_pwm);
  motor_2.writeMicroseconds (max_pwm);
  motor_3.writeMicroseconds (max_pwm);
  motor_4.writeMicroseconds (max_pwm);
  delay (2000);
  motor_1.writeMicroseconds (min_pwm);
  motor_2.writeMicroseconds (min_pwm);
  motor_3.writeMicroseconds (min_pwm);
  motor_4.writeMicroseconds (min_pwm);
  delay (4000);
}

void loop()
{
 /* int a = 100;
  motor_1.writeMicroseconds (1293 );
  motor_2.writeMicroseconds (1324 );
  motor_3.writeMicroseconds (1291 );
  motor_4.writeMicroseconds (1310 );
  delay (2000);
 
  STOP (10000);*/
  
  for (int x = 1000; x < 1100 ; x++)
  {
    motor_1.writeMicroseconds (x);
    motor_2.writeMicroseconds (x);
    motor_3.writeMicroseconds (x);
    motor_4.writeMicroseconds (x);

    delay (50);
  }
  /*for (int x = 1700; x > 800; x--)
  {
    motor_1.writeMicroseconds (x);
    motor_2.writeMicroseconds (x);
    motor_3.writeMicroseconds (x);
    motor_4.writeMicroseconds (x);

    delay (50);
  }
  motor_1.writeMicroseconds (0);
  motor_2.writeMicroseconds (0);
  motor_3.writeMicroseconds (0);
  motor_4.writeMicroseconds (0);
  delay (5000);*/
}
