//////////Robot//////////
const int EnLSpeed = 5;
const int EnLFloat = 3;
const int EnRSpeed = 9;
const int EnRFloat = 6;
//////////Motor_move//////////
const int L3 = 7;
const int L4 = 8;
const int R1 = 10;
const int R2 = 11;
//////////Motor_float//////////
const int L1 = 2;
const int L2 = 4;
const int R3 = 12;
const int R4 = 13;
//////////Control//////////
const int ConX = A2;
const int ConY = A3;
const int ConBUT = A1;
///////////////////////////////////
//          function            //
//////////////////////////////////
void forvard (int t)
{
  analogWrite (EnLSpeed, 255);
  digitalWrite (L3, HIGH);
  digitalWrite (L4, LOW);
  analogWrite (EnRSpeed, 255);
  digitalWrite (R1, HIGH);
  digitalWrite (R2, LOW);
  delay (t);
}
void backvard (int t)
{
  analogWrite (EnLSpeed, 255);
  digitalWrite (L3, LOW);
  digitalWrite (L4, HIGH);
  analogWrite (EnRSpeed, 255);
  digitalWrite (R1, LOW);
  digitalWrite (R2, HIGH);
  delay (t);
}
void right (int t)
{
  analogWrite (EnLSpeed, 255);
  digitalWrite (L3, LOW);
  digitalWrite (L4, HIGH);
  analogWrite (EnRSpeed, 255);
  digitalWrite (R1, HIGH);
  digitalWrite (R2, LOW);
  delay (t);
}
void left (int t)
{
  analogWrite (EnLSpeed, 255);
  digitalWrite (L3, HIGH);
  digitalWrite (L4, LOW);
  analogWrite (EnRSpeed, 255);
  digitalWrite (R1, LOW);
  digitalWrite (R2, HIGH);
  delay (t);
}
void up (int t)
{
  analogWrite (EnLSpeed, 255);
  digitalWrite (L2, HIGH);
  digitalWrite (L1, LOW);
  analogWrite (EnRSpeed, 255);
  digitalWrite (R2, HIGH);
  digitalWrite (R1, LOW);
  delay (t);
}
void down (int t)
{
  analogWrite (EnLSpeed, 255);
  digitalWrite (L1, HIGH);
  digitalWrite (L2, LOW);
  analogWrite (EnRSpeed, 255);
  digitalWrite (R1, HIGH);
  digitalWrite (R2, LOW);
  delay (t);
}
void Stop (int t)
{
  analogWrite (EnLSpeed, 0);
  digitalWrite (L1, LOW);
  digitalWrite (L2, LOW);
  digitalWrite (L3, LOW);
  digitalWrite (L4, LOW);
  analogWrite (EnRSpeed, 0);
  digitalWrite (R1, LOW);
  digitalWrite (R2, LOW);
  digitalWrite (R3, LOW);
  digitalWrite (R4, LOW);
  delay (t);
}
void setup() {
  Serial.begin (9600);
  pinMode (EnLSpeed, OUTPUT);
  pinMode (EnLFloat, OUTPUT);
  pinMode (EnRSpeed, OUTPUT);
  pinMode (EnRFloat, OUTPUT);
  pinMode (L1, OUTPUT);
  pinMode (L2, OUTPUT);
  pinMode (L3, OUTPUT);
  pinMode (L4, OUTPUT);
  pinMode (R1, OUTPUT);
  pinMode (R2, OUTPUT);
  pinMode (R3, OUTPUT);
  pinMode (R4, OUTPUT);
  pinMode (ConX, INPUT);
  pinMode (ConY, INPUT);
  pinMode (ConBUT, INPUT);
}

void loop() {
  int ConX_state = analogRead (ConX);
  int ConY_state = analogRead (ConY);
  int ConBUT_state = analogRead (ConBUT);

  Serial.print ("ConX_state = ");
  Serial.println (ConX_state);
  Serial.print ("ConY_state = ");
  Serial.println (ConY_state);
  Serial.print ("ConBUT_state = ");
  Serial.println (ConBUT_state);
  Serial.println ("");
  delay (500);
  if (ConBUT_state < 50)
  {
    Serial.println ("!UP!");
    up (10);
  }
  else if ((ConBUT_state > 700) && (ConBUT_state < 800))
  {
    Serial.println ("!DOWN!");
    down (10);
  }
  else if (ConBUT_state > 1000)
  {
    Serial.println ("!Stop!");
    Stop (10);
  }
}
