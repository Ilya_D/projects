#include <Wire.h>
#include <MPU6050.h> // библиотека для работы с датчиком MPU6050 (ссылка для ее скачивания приведена в тексте статьи)
//#include "MPU6050.h"
#define period 10000
MPU6050 mpu;
int count = 0;
char okFlag = 0;

int speed_uping_m1 = 1400;
int speed_uping_m2 = 1400;
int speed_uping_m3 = 1400;
int speed_uping_m4 = 1400;

#include <Servo.h>
Servo motor_1;
Servo motor_2;
Servo motor_3;
Servo motor_4;
const int mot_pin_4 = 10;
const int mot_pin_3 = 6;
const int mot_pin_2 = 5;
const int mot_pin_1 = 3;
const int max_pwm = 1700;
const int min_pwm = 850;

void stabilization_uping (int t)  // функция стабилизированного подъема
{
  for (int i = 0; i < t; i++)
  {
    //установка оборотов на взлет
    speed_uping_m1 = 1400;
    speed_uping_m2 = 1400;
    speed_uping_m3 = 1400;
    speed_uping_m4 = 1400;
    motor_1.writeMicroseconds (speed_uping_m1);
    motor_2.writeMicroseconds (speed_uping_m2);
    motor_3.writeMicroseconds (speed_uping_m3);
    motor_4.writeMicroseconds (speed_uping_m4);

    // проверка координат
    long st = millis();
    Vector rawAccel = mpu.readRawAccel();
    Vector normAccel = mpu.readNormalizeAccel();
    float X = normAccel.XAxis - 1.1;
    float Y = normAccel.YAxis - 0.2;
    float Z = normAccel.ZAxis - 9.4;
    delay (1);
    // выравнивание
  
      long st = millis();
      Vector rawAccel = mpu.readRawAccel();
      Vector normAccel = mpu.readNormalizeAccel();
      float X = normAccel.XAxis - 1.1;
      float Y = normAccel.YAxis - 0.2;
      float Z = normAccel.ZAxis - 9.4;
      motor_1.writeMicroseconds (speed_uping_m1);
      motor_2.writeMicroseconds (speed_uping_m2++);
      motor_3.writeMicroseconds (speed_uping_m3);
      motor_4.writeMicroseconds (speed_uping_m4++);
      delay (1);
      i++;    
    
      long st = millis();
      Vector rawAccel = mpu.readRawAccel();
      Vector normAccel = mpu.readNormalizeAccel();
      float X = normAccel.XAxis - 1.1;
      float Y = normAccel.YAxis - 0.2;
      float Z = normAccel.ZAxis - 9.4;
      motor_1.writeMicroseconds (speed_uping_m1++);
      motor_2.writeMicroseconds (speed_uping_m2);
      motor_3.writeMicroseconds (speed_uping_m3++);
      motor_4.writeMicroseconds (speed_uping_m4);
      delay (1);
      i++;
  
      long st = millis();
      Vector rawAccel = mpu.readRawAccel();
      Vector normAccel = mpu.readNormalizeAccel();
      float X = normAccel.XAxis - 1.1;
      float Y = normAccel.YAxis - 0.2;
      float Z = normAccel.ZAxis - 9.4;
      motor_1.writeMicroseconds (speed_uping_m1);
      motor_2.writeMicroseconds (speed_uping_m2);
      motor_3.writeMicroseconds (speed_uping_m3++);
      motor_4.writeMicroseconds (speed_uping_m4++);
      delay (1);
      i++;  
  
      long st = millis();
      Vector rawAccel = mpu.readRawAccel();
      Vector normAccel = mpu.readNormalizeAccel();
      float X = normAccel.XAxis - 1.1;
      float Y = normAccel.YAxis - 0.2;
      float Z = normAccel.ZAxis - 9.4;
      motor_1.writeMicroseconds (speed_uping_m1++);
      motor_2.writeMicroseconds (speed_uping_m2++);
      motor_3.writeMicroseconds (speed_uping_m3);
      motor_4.writeMicroseconds (speed_uping_m4);
      delay (1);
      i++;   
    

    ///////////////////////////////////////////////

      long st = millis();
      Vector rawAccel = mpu.readRawAccel();
      Vector normAccel = mpu.readNormalizeAccel();
      float X = normAccel.XAxis - 1.1;
      float Y = normAccel.YAxis - 0.2;
      float Z = normAccel.ZAxis - 9.4;
      motor_1.writeMicroseconds (speed_uping_m1++);
      motor_2.writeMicroseconds (speed_uping_m2++);
      motor_3.writeMicroseconds (speed_uping_m3);
      motor_4.writeMicroseconds (speed_uping_m4++);   
      delay (1);
    i++;
    
      long st = millis();
      Vector rawAccel = mpu.readRawAccel();
      Vector normAccel = mpu.readNormalizeAccel();
      float X = normAccel.XAxis - 1.1;
      float Y = normAccel.YAxis - 0.2;
      float Z = normAccel.ZAxis - 9.4;
      motor_1.writeMicroseconds (speed_uping_m1++);
      motor_2.writeMicroseconds (speed_uping_m2);
      motor_3.writeMicroseconds (speed_uping_m3++);
      motor_4.writeMicroseconds (speed_uping_m4++);   
      delay (1);
    i++;
    
      long st = millis();
      Vector rawAccel = mpu.readRawAccel();
      Vector normAccel = mpu.readNormalizeAccel();
      float X = normAccel.XAxis - 1.1;
      float Y = normAccel.YAxis - 0.2;
      float Z = normAccel.ZAxis - 9.4;
      motor_1.writeMicroseconds (speed_uping_m1);
      motor_2.writeMicroseconds (speed_uping_m2++);
      motor_3.writeMicroseconds (speed_uping_m3++);
      motor_4.writeMicroseconds (speed_uping_m4++);   
      delay (1);
    i++;
    
      long st = millis();
      Vector rawAccel = mpu.readRawAccel();
      Vector normAccel = mpu.readNormalizeAccel();
      float X = normAccel.XAxis - 1.1;
      float Y = normAccel.YAxis - 0.2;
      float Z = normAccel.ZAxis - 9.4;
      motor_1.writeMicroseconds (speed_uping_m1++);
      motor_2.writeMicroseconds (speed_uping_m2++);
      motor_3.writeMicroseconds (speed_uping_m3++);
      motor_4.writeMicroseconds (speed_uping_m4);
      delay (1);
    i++;
    
  }
}

void STOP_motor (int t)
{
  motor_1.writeMicroseconds (0);
  motor_2.writeMicroseconds (0);
  motor_3.writeMicroseconds (0);
  motor_4.writeMicroseconds (0);
  delay (t);
}
void tempShow() //данные температуры
{
  float temp = mpu.readTemperature();
  Serial.print(" Temp = ");
  Serial.print(temp);
  Serial.println(" *C");
  delay(400);
}
void gyroShow() //данные гироскопа
{
  //lcd.setCursor(0,0);
  Vector rawGyro = mpu.readRawGyro();
  Vector normGyro = mpu.readNormalizeGyro();

  Serial.print(" Xnorm = ");
  Serial.print(normGyro.XAxis);
  Serial.print(" Ynorm = ");
  Serial.print(normGyro.YAxis);
  Serial.print(" Znorm = ");
  Serial.println(normGyro.ZAxis);
  delay(200);
}
int accelShow() // данные акселерометра
{
  // lcd.setCursor(0,0);
  Vector rawAccel = mpu.readRawAccel();
  Vector normAccel = mpu.readNormalizeAccel();
  Serial.print(" Xnorm = ");
  Serial.print(normAccel.XAxis - 1.1);
  Serial.print(" Ynorm = ");
  Serial.print(normAccel.YAxis - 0.2);
  Serial.print(" Znorm = ");
  Serial.println(normAccel.ZAxis - 9.4);
  return normAccel.XAxis, normAccel.YAxis, normAccel.ZAxis;
  delay(200);
}
void setup()
{
  Serial.begin(9600); // инициализируем последовательный порт для работы на скорости 9600 бод/с
  Serial.println("Initialize MPU6050");
  while (!mpu.begin(MPU6050_SCALE_2000DPS, MPU6050_RANGE_2G))
  { //если нет соединения с MPU6050, выдаем предупреждающие сообщения
    Serial.println("Could not find a valid MPU6050 sensor, check wiring!");
    delay(500);
  }
  count = 0;
  mpu.calibrateGyro(); // калибровка гироскопа
  mpu.setThreshold(3);

  motor_1.attach (mot_pin_1);
  motor_2.attach (mot_pin_2);
  motor_3.attach (mot_pin_3);
  motor_4.attach (mot_pin_4);
  //delay (1000);
  motor_1.writeMicroseconds (max_pwm);
  motor_2.writeMicroseconds (max_pwm);
  motor_3.writeMicroseconds (max_pwm);
  motor_4.writeMicroseconds (max_pwm);
  delay (2000);
  motor_1.writeMicroseconds (min_pwm);
  motor_2.writeMicroseconds (min_pwm);
  motor_3.writeMicroseconds (min_pwm);
  motor_4.writeMicroseconds (min_pwm);
  delay (4000);
}
void loop()
{
  for (int x = 800; x < 1400 ; x=x+2)
  {
    motor_1.writeMicroseconds (x);
    motor_2.writeMicroseconds (x);
    motor_3.writeMicroseconds (x);
    motor_4.writeMicroseconds (x);
    delay (3);
  }
  stabilization_uping (500);
  STOP_motor (5000);


  /*Serial.println("Temperature");
    while(millis()<st+period)
    {
    lcd.setCursor(0,1);
    tempShow();
    }
    delay(2000);
    st=millis();
    Serial.println("Gyro");
    while(millis()<st+period)
    {
    lcd.setCursor(0,1);
    gyroShow();
    }
    //delay(2000);
    st=millis();
    Serial.println("Accelerometer");
    while(millis()<st+period)
    {
    lcd.setCursor(0,1);
    accelShow();
    }*/
  // взлет
  /*int a = 50;
    if ((normAccel.XAxis - 1.1) < 0 && (normAccel.YAxis-0.2) ==0)
    {
    motor_1.writeMicroseconds (1300 );
    motor_2.writeMicroseconds (1300 );
    motor_3.writeMicroseconds (1300 + a);
    motor_4.writeMicroseconds (1300 );
    }*/


}
