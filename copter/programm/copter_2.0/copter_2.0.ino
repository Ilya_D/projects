#include <Servo.h>
Servo motor_1;
Servo motor_2;
Servo motor_3;
Servo motor_4;
const int mot_pin_4 = 9;
const int mot_pin_3 = 6;
const int mot_pin_2 = 5;
const int mot_pin_1 = 3;
const int max_pwm = 1000;
const int min_pwm = 850;

void STOP (int t)
{
  motor_1.writeMicroseconds (0);
  motor_2.writeMicroseconds (0);
  motor_3.writeMicroseconds (0);
  motor_4.writeMicroseconds (0);
  delay (t);
}
/*void demostioshon(int t)
{
  motor_1.writeMicroseconds (650);
  motor_2.writeMicroseconds (650);
  motor_3.writeMicroseconds (650);
  motor_4.writeMicroseconds (650);
  delay (t);
}*/
void setup()
{
  Serial.begin (9600);
  motor_1.attach (mot_pin_1);
  motor_2.attach (mot_pin_2);
  motor_3.attach (mot_pin_3);
  motor_4.attach (mot_pin_4);
  delay (1000);
  motor_1.writeMicroseconds (min_pwm);
  motor_2.writeMicroseconds (min_pwm);
  motor_3.writeMicroseconds (min_pwm);
  motor_4.writeMicroseconds (min_pwm);
  delay (2000);
  motor_1.writeMicroseconds (max_pwm);
  motor_2.writeMicroseconds (max_pwm);
  motor_3.writeMicroseconds (max_pwm);
  motor_4.writeMicroseconds (max_pwm);
  delay (2000);
}


void loop()
{
  // int a = 100;
  // motor_1.writeMicroseconds (1293 );
  // motor_2.writeMicroseconds (1324 );
  // motor_3.writeMicroseconds (1291 );
  // motor_4.writeMicroseconds (1310 );
  // delay (2000);
  //motor_1.writeMicroseconds (1293 - a);
  //motor_2.writeMicroseconds (1324 - a);
  //motor_3.writeMicroseconds (1291 - a);
  //motor_4.writeMicroseconds (1310 - a);
  //delay (1000);
  // STOP (10000)
/*for (int x = 800; x < 850 ; x++)
  {
    motor_1.writeMicroseconds (x);
    motor_2.writeMicroseconds (x);
    motor_3.writeMicroseconds (x);
    motor_4.writeMicroseconds (x);

    delay (100);

  }*/
    motor_1.writeMicroseconds (1000);
    motor_2.writeMicroseconds (1000);
    motor_3.writeMicroseconds (1000);
    motor_4.writeMicroseconds (1000);
    delay (10000);

}
