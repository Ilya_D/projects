// Basic demo for accelerometer readings from Adafruit MPU6050

#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>

Adafruit_MPU6050 mpu;

const int ZUM_ZUM = 11;  //зумер
int chet = 0;

// моторы
#include <Servo.h>
Servo motor_1;
Servo motor_2;
Servo motor_3;
Servo motor_4;
const int mot_pin_4 = 10;
const int mot_pin_3 = 6;
const int mot_pin_2 = 5;
const int mot_pin_1 = 3;
const int max_pwm = 1700;
const int min_pwm = 850;

int motor1_stab_speed = 1000;
int motor2_stab_speed = 1000;
int motor3_stab_speed = 1000;
int motor4_stab_speed = 1000;
int speed_uping = 1450;
int motor_stab_speed_max = 1350;
int speed_stabilizator = 1000;
int viravnivanie = 10;
bool k = false;

void stabilizator() {  // функция для стабилизации
  if (!k) {
    for (int x = min_pwm; x < speed_stabilizator; x++) {  //разгон моторов для стабилизации
      motor_1.writeMicroseconds(x);
      motor_2.writeMicroseconds(x);
      motor_3.writeMicroseconds(x);
      motor_4.writeMicroseconds(x);
    }
    k = true;
    sensors_event_t a, g, temp;
    mpu.getEvent(&a, &g, &temp);
    while (true) {
      sensors_event_t a, g, temp;
      mpu.getEvent(&a, &g, &temp);
      if (a.acceleration.x > -0.05 && a.acceleration.x < 0.05 && a.acceleration.y > -0.05 && a.acceleration.y < 0.05) {
        break;
      }
      if (motor1_stab_speed > motor_stab_speed_max || motor2_stab_speed > motor_stab_speed_max || motor3_stab_speed > motor_stab_speed_max || motor4_stab_speed > motor_stab_speed_max) {
        // если скорости моторов доходят до скорости подъема
        if (a.acceleration.x > 0.05) {
          motor_1.writeMicroseconds(motor1_stab_speed--);
          motor_2.writeMicroseconds(motor2_stab_speed);
          motor_3.writeMicroseconds(motor3_stab_speed--);
          motor_4.writeMicroseconds(motor4_stab_speed);
        } else if (a.acceleration.x < -0.05) {
          motor_1.writeMicroseconds(motor1_stab_speed);
          motor_2.writeMicroseconds(motor2_stab_speed--);
          motor_3.writeMicroseconds(motor3_stab_speed);
          motor_4.writeMicroseconds(motor4_stab_speed--);
        } else if (a.acceleration.y > 0.05) {
          motor_1.writeMicroseconds(motor1_stab_speed--);
          motor_2.writeMicroseconds(motor2_stab_speed--);
          motor_3.writeMicroseconds(motor3_stab_speed);
          motor_4.writeMicroseconds(motor4_stab_speed);
        } else if (a.acceleration.y < -0.05) {
          motor_1.writeMicroseconds(motor1_stab_speed);
          motor_2.writeMicroseconds(motor2_stab_speed);
          motor_3.writeMicroseconds(motor3_stab_speed--);
          motor_4.writeMicroseconds(motor4_stab_speed--);
        }
      }
      if (a.acceleration.x > 0.05) {
        motor_1.writeMicroseconds(motor1_stab_speed);
        motor_2.writeMicroseconds(motor2_stab_speed++);
        motor_3.writeMicroseconds(motor3_stab_speed);
        motor_4.writeMicroseconds(motor4_stab_speed++);
      } else if (a.acceleration.x < -0.05) {
        motor_1.writeMicroseconds(motor1_stab_speed++);
        motor_2.writeMicroseconds(motor2_stab_speed);
        motor_3.writeMicroseconds(motor3_stab_speed++);
        motor_4.writeMicroseconds(motor4_stab_speed);
      } else if (a.acceleration.y > 0.05) {
        motor_1.writeMicroseconds(motor1_stab_speed++);
        motor_2.writeMicroseconds(motor2_stab_speed++);
        motor_3.writeMicroseconds(motor3_stab_speed);
        motor_4.writeMicroseconds(motor4_stab_speed);
      } else if (a.acceleration.y < -0.05) {
        motor_1.writeMicroseconds(motor1_stab_speed);
        motor_2.writeMicroseconds(motor2_stab_speed);
        motor_3.writeMicroseconds(motor3_stab_speed++);
        motor_4.writeMicroseconds(motor4_stab_speed++);
      }
      //Serial.println("стабилизируюсь...");
      delay(25);
    }
  }
}
void STOP_motor(int t) {
  motor_1.writeMicroseconds(0);
  motor_2.writeMicroseconds(0);
  motor_3.writeMicroseconds(0);
  motor_4.writeMicroseconds(0);
  delay(t);
}

void overclocking() {  //функция раскрутки моторов
  for (int x = min_pwm; x < speed_uping; x++) {
    motor_1.writeMicroseconds(x);
    motor_2.writeMicroseconds(x);
    motor_3.writeMicroseconds(x);
    motor_4.writeMicroseconds(x);
    delay(5);
  }
}
void setup(void) {
  pinMode(ZUM_ZUM, OUTPUT);

  motor_1.attach(mot_pin_1);
  motor_2.attach(mot_pin_2);
  motor_3.attach(mot_pin_3);
  motor_4.attach(mot_pin_4);
  //delay (1000);
  motor_1.writeMicroseconds(max_pwm);
  motor_2.writeMicroseconds(max_pwm);
  motor_3.writeMicroseconds(max_pwm);
  motor_4.writeMicroseconds(max_pwm);
  delay(2000);
  motor_1.writeMicroseconds(min_pwm);
  motor_2.writeMicroseconds(min_pwm);
  motor_3.writeMicroseconds(min_pwm);
  motor_4.writeMicroseconds(min_pwm);
  delay(4000);

  Serial.begin(115200);
  while (!Serial)
    delay(10);  // will pause Zero, Leonardo, etc until serial console opens

  Serial.println("Adafruit MPU6050 test!");

  // Try to initialize!
  if (!mpu.begin()) {
    Serial.println("Failed to find MPU6050 chip");
    while (1) {
      delay(10);
    }
  }
  Serial.println("MPU6050 Found!");

  mpu.setAccelerometerRange(MPU6050_RANGE_8_G);
  Serial.print("Accelerometer range set to: ");
  switch (mpu.getAccelerometerRange()) {
    case MPU6050_RANGE_2_G:
      Serial.println("+-2G");
      break;
    case MPU6050_RANGE_4_G:
      Serial.println("+-4G");
      break;
    case MPU6050_RANGE_8_G:
      Serial.println("+-8G");
      break;
    case MPU6050_RANGE_16_G:
      Serial.println("+-16G");
      break;
  }
  mpu.setGyroRange(MPU6050_RANGE_500_DEG);
  Serial.print("Gyro range set to: ");
  switch (mpu.getGyroRange()) {
    case MPU6050_RANGE_250_DEG:
      Serial.println("+- 250 deg/s");
      break;
    case MPU6050_RANGE_500_DEG:
      Serial.println("+- 500 deg/s");
      break;
    case MPU6050_RANGE_1000_DEG:
      Serial.println("+- 1000 deg/s");
      break;
    case MPU6050_RANGE_2000_DEG:
      Serial.println("+- 2000 deg/s");
      break;
  }

  mpu.setFilterBandwidth(MPU6050_BAND_21_HZ);
  Serial.print("Filter bandwidth set to: ");
  switch (mpu.getFilterBandwidth()) {
    case MPU6050_BAND_260_HZ:
      Serial.println("260 Hz");
      break;
    case MPU6050_BAND_184_HZ:
      Serial.println("184 Hz");
      break;
    case MPU6050_BAND_94_HZ:
      Serial.println("94 Hz");
      break;
    case MPU6050_BAND_44_HZ:
      Serial.println("44 Hz");
      break;
    case MPU6050_BAND_21_HZ:
      Serial.println("21 Hz");
      break;
    case MPU6050_BAND_10_HZ:
      Serial.println("10 Hz");
      break;
    case MPU6050_BAND_5_HZ:
      Serial.println("5 Hz");
      break;
  }

  Serial.println("");
  delay(100);
  digitalWrite(ZUM_ZUM, HIGH);
  delay(500);
  digitalWrite(ZUM_ZUM, LOW);
  delay(500);
  digitalWrite(ZUM_ZUM, HIGH);
  delay(500);
  digitalWrite(ZUM_ZUM, LOW);
  delay(500);
  digitalWrite(ZUM_ZUM, HIGH);
  delay(500);
  digitalWrite(ZUM_ZUM, LOW);
  //overclocking();
  digitalWrite(ZUM_ZUM, HIGH);
  delay(500);
  digitalWrite(ZUM_ZUM, LOW);

  stabilizator();
}

void loop() {

  /* Get new sensor events with the readings */
  sensors_event_t a, g, temp;
  mpu.getEvent(&a, &g, &temp);

  /* Print out the values */
  Serial.print("Acceleration X: ");
  Serial.print(a.acceleration.x);
  Serial.print(", Y: ");
  Serial.print(a.acceleration.y);
  Serial.print(", Z: ");
  Serial.print(a.acceleration.z);
  Serial.println(" m/s^2");

  Serial.print("Rotation X: ");
  Serial.print(g.gyro.x);
  Serial.print(", Y: ");
  Serial.print(g.gyro.y);
  Serial.print(", Z: ");
  Serial.print(g.gyro.z);
  Serial.println(" rad/s");

  Serial.print("Temperature: ");
  Serial.print(temp.temperature);
  Serial.println(" degC");

  Serial.println("");
  // delay(500);
  //стабилизированный подъем
  //chet++;
  digitalWrite(7, 1);
  //for (int x = 0; x < 100; x++) {  //плавный разго для подъема
    motor_1.writeMicroseconds(motor1_stab_speed+100);
    motor_2.writeMicroseconds(motor2_stab_speed+100);
    motor_3.writeMicroseconds(motor3_stab_speed+100);
    motor_4.writeMicroseconds(motor4_stab_speed+100);
   // delay(5);
 // }
  delay(500);
  STOP_motor(5000);


  /*
  if (a.acceleration.x > 0, 15) {
    motor_1.writeMicroseconds(speed_uping - viravnivanie);
    motor_2.writeMicroseconds(speed_uping);
    motor_3.writeMicroseconds(speed_uping - viravnivanie);
    motor_4.writeMicroseconds(speed_uping);
  }
  if (a.acceleration.x < -0, 15) {
    motor_1.writeMicroseconds(speed_uping);
    motor_2.writeMicroseconds(speed_uping - viravnivanie);
    motor_3.writeMicroseconds(speed_uping);
    motor_4.writeMicroseconds(speed_uping - viravnivanie);
  }
  if (a.acceleration.y > 0, 15) {
    motor_1.writeMicroseconds(speed_uping);
    motor_2.writeMicroseconds(speed_uping);
    motor_3.writeMicroseconds(speed_uping - viravnivanie);
    motor_4.writeMicroseconds(speed_uping - viravnivanie);
  }
  if (a.acceleration.y < -0, 15) {
    motor_1.writeMicroseconds(speed_uping - viravnivanie);
    motor_2.writeMicroseconds(speed_uping - viravnivanie);
    motor_3.writeMicroseconds(speed_uping);
    motor_4.writeMicroseconds(speed_uping);
  }
  if (chet == 200) {
  digitalWrite(ZUM_ZUM, HIGH);
  delay(500);
  digitalWrite(ZUM_ZUM, LOW);
  STOP_motor(5000);
  }*/
}