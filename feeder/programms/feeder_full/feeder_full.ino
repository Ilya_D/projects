#include <ThreeWire.h>
#include <RtcDS1302.h> // Подключаем библиотеку ThreeWire

const int DIR = 5;
const int STEP = 2;
const int ENABLE = 8;
const int STARTING_SPEED = 10000;  // начальная скорость ускорения (задержка между импульсами на мотор)
const int ACCELERATION = 1000;      // ускорение (уменьшение задержки между импульсами)
const int SPEED_MOTOR = 700;       // скорость вращения мотора (задержка между импульсами на мотор)
const long int AMOUNT_OF_FEED = 10000;   // количество корма (сколько вращается мотор)
int ch = 0;
int trigPin = 12;
int echoPin = 13;
const int t_korm = 54;
bool cat = true;
ThreeWire myWire(7, 6, 4);                            // Указываем вывода IO, SCLK, CE
//clock желтый провод 6 пин
//data оранжевый провод 7 пин
//reset зеленый провод 4 пин
RtcDS1302<ThreeWire> Rtc(myWire);

void acceleration_tuda ()
{
  digitalWrite(ENABLE, LOW);
  digitalWrite(DIR, LOW); // Устанавливаем направление
  for (int i = STARTING_SPEED; i > SPEED_MOTOR; i = i - ACCELERATION)
  {
    digitalWrite(STEP, LOW);
    digitalWrite(STEP, HIGH); // В этих двух строках LOW и HIGH дается команда шаговому двигателю двигатьс
    delayMicroseconds(i); // Эта задержка соответствует максимальной скорости данного конкретного шагового двигателя
  }
}
void acceleration_suda ()
{
  digitalWrite(ENABLE, LOW);
  digitalWrite(DIR, HIGH); // Устанавливаем направление
  for (int i = STARTING_SPEED; i > SPEED_MOTOR; i = i - ACCELERATION)
  {
    digitalWrite(STEP, LOW);
    digitalWrite(STEP, HIGH); // В этих двух строках LOW и HIGH дается команда шаговому двигателю двигатьс
    delayMicroseconds(i); // Эта задержка соответствует максимальной скорости данного конкретного шагового двигателя
  }
}
void motor_tuda ()
{
  digitalWrite(ENABLE, LOW);
  digitalWrite(DIR, LOW); // Устанавливаем направление
  for (int i = 0; i < AMOUNT_OF_FEED; i++)
  {
    digitalWrite(STEP, LOW);
    digitalWrite(STEP, HIGH); // В этих двух строках LOW и HIGH дается команда шаговому двигателю двигатьс
    delayMicroseconds(SPEED_MOTOR); // Эта задержка соответствует максимальной скорости данного конкретного шагового двигателя
  }
}
void motor_suda ()
{
  digitalWrite(ENABLE, LOW);
  digitalWrite(DIR, HIGH); // Устанавливаем направление
  for (int i = 0; i < AMOUNT_OF_FEED; i++)
  {
    digitalWrite(STEP, LOW);
    digitalWrite(STEP, HIGH); // В этих двух строках LOW и HIGH дается команда шаговому двигателю двигатьс
    delayMicroseconds(SPEED_MOTOR); // Эта задержка соответствует максимальной скорости данного конкретного шагового двигателя
  }
}

void shneck_tuda ()
{
  acceleration_tuda ();
  motor_tuda ();
  digitalWrite(ENABLE, HIGH);
}
void shneck_suda ()
{
  acceleration_suda ();
  motor_suda ();
  digitalWrite(ENABLE, HIGH);
}

void setup ()
{
  Serial.begin(9600);                              // Установка последовательной связи на скорости 9600
  Rtc.Begin();                                     // Инициализация RTC
  RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__); // Копирование даты и времени в compiled
  Rtc.SetDateTime(compiled);                       // Установка времени
  pinMode (DIR, OUTPUT);
  pinMode (STEP, OUTPUT);
  pinMode (ENABLE, OUTPUT);
  Serial.begin (9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

}

void loop ()
{
  int duration, distance;
  // для большей точности установим значение LOW на пине Trig
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Теперь установим высокий уровень на пине Trig
  digitalWrite(trigPin, HIGH);
  // Подождем 10 μs
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);// Узнаем длительность высокого сигнала на пине Echo
  duration = pulseIn(echoPin, HIGH);
  // Рассчитаем расстояние
  distance = duration / 58;
  // Выведем значение в Serial Monitor
  Serial.print(distance);
  Serial.println(" cm");
  delay(100);
  Rtc.GetDateTime();
  RtcDateTime now = Rtc.GetDateTime();
  Serial.print("DATA - ");                           // Отправка данных на последовательный порт
  Serial.print(now.Month());                         // Отправка месяца
  Serial.print(".");                                 // Отправка данных на последовательный порт
  Serial.print(now.Day());                           // Отправка дня
  Serial.print(".");                                 // Отправка данных на последовательный порт
  Serial.print(now.Year());                          // Отправка года
  Serial.print(" TIME - ");                          // Отправка данных на последовательный порт
  Serial.print(now.Hour());                          // Отправка часа
  Serial.print(":");                                 // Отправка данных на последовательный порт
  Serial.print(now.Minute());                        // Отправка минут
  Serial.print(":");                                 // Отправка данных на последовательный порт
  Serial.println(now.Second());                      // Отправка секунд
  delay(1000);
  if (distance < 10 && distance != 0)
  {
    delay (5000);
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH); // Теперь установим высокий уровень на пине Trig
    delayMicroseconds(10);// Подождем 10 μs
    digitalWrite(trigPin, LOW);// Узнаем длительность высокого сигнала на пине Echo
    duration = pulseIn(echoPin, HIGH);// Рассчитаем расстояние
    distance = duration / 58;
    if (distance < 10 && distance != 0)
    {
      cat = true;
    }

  }
  if (now.Minute() % 2 != 0)
  {
    ch = 0;
  }
  if ((now.Minute() % 1 == 0) && ch == 0 && cat)
  {
    ch++;
    shneck_suda ();
    cat = false;
  }
}
