#include <Wire.h>                      // подключение библиотеки i2c протокола
#include <LiquidCrystal_I2C.h>         // Подключение библиотеки экрана
#include <LCD_1602_RUS.h>              // подключаем библиотеку LCD_1602_RUS
LCD_1602_RUS LCD(0x27, 16, 2);         // присваиваем имя LCD для дисплея
//LiquidCrystal_I2C lcd(0x27, 16, 2);    // Указываем I2C адрес
const int button1 = 7;                 // пин кнопки1
const int button2 = 4;                 // пин кнопки 2
const int button3 = 2;                 // пин кнопки 3
int number_push = 0;
int kol_vo_korm = 0;

void setup() {
  LCD.init();                          // Инициализация дисплея
  LCD.backlight();                     // включение подсветки
  Serial.begin (9600);                 // устанавлиаем скорость общения через монитор порта для отладки программы
  LCD.setCursor(4, 0);                 // устанавливаем курсор экрана на первый символ первой строки
  LCD.print("кормушка");         // Набор текста на первой строке
  LCD.setCursor(4, 1);
  LCD.print("включена");
  pinMode (button1, INPUT);            // инициализируем данный пин как "вход сигнала"
  pinMode (button2, INPUT);            // инициализируем данный пин как "вход сигнала"
  pinMode (button3, INPUT);            // инициализируем данный пин как "вход сигнала"
}

void loop() {
  bool button1_state = digitalRead (button1);        // чтение состояния кнопки1
  bool button2_state = digitalRead (button2);        // чтение состояния кнопки2
  bool button3_state = digitalRead (button3);        // чтение состояния кнопки3
  Serial.print ("  кнопка №1 = ");
  Serial.print (button1_state);
  Serial.print ("  кнопка №2 = ");
  Serial.print (button2_state);
  Serial.print ("  кнопка №3 = ");
  Serial.println (button3_state);
  delay (500);
  if (button3_state && number_push == 0) {            // нажили 1й раз кнопку №3
    LCD.clear();
    LCD.setCursor(0, 0);
    LCD.print("1й пункт меню");
    number_push++;
    button3_state = false;
    delay (1000);
  }
  if (button3_state && number_push == 1) {            // нажили 2й раз кнопку №3
    LCD.clear();
    LCD.setCursor(0, 0);
    LCD.print("2й пункт меню");
    number_push++;
    button3_state = false;
    delay (1000);
  }
  if (button3_state && number_push == 2) {            // нажили 3й раз кнопку №3
    LCD.clear();
    LCD.setCursor(0, 0);
    LCD.print("3й пункт меню");
    number_push++;
    button3_state = false;
    delay (1000);
  }
  if (button3_state && number_push == 3) {            // нажили 4й раз кнопку №3
    LCD.clear();
    LCD.setCursor(4, 0);                 // устанавливаем курсор экрана на первый символ первой строки
    LCD.print("кормушка");         // Набор текста на первой строке
    LCD.setCursor(4, 1);
    LCD.print("включена");
    number_push = 0;
    button3_state = false;
    delay (1000);
  }
}
