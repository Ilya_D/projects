#include <NewPing.h>
#include <Servo.h>

// моторы
const int L_motor_F = 8;        // левые моторы вперед
const int L_motor_B = 7;        // левые моторы назад
const int R_motor_F = 2;        // правые моторы вперед
const int R_motor_B = 4;        // правые моторы назад
const int L_motor_speed = 6;    // скорость левых моторов
const int R_motor_speed = 5;    // скорость правых моторов

// датчики чтения линии
const int L = A2;               // левый датчик
const int LC = A1;              // левый центральный датчик
const int RC = A3;              // правый центральный датчик
const int R = A4;               // правый датчик

// сонар
const int EchoPin = 13;
const int TrigPin = 12;
const int MAX_DISTANCE = 50;
NewPing sonar (EchoPin, TrigPin, MAX_DISTANCE);

// Серво
Servo Sera;

// переменные скоростей
int R_speed = 60;
int L_speed = 60;

void forward (int t) // функция движения вперед
{
  digitalWrite (R_motor_F, HIGH);
  digitalWrite (L_motor_F, HIGH);
  digitalWrite (R_motor_B, LOW);
  digitalWrite (L_motor_B, LOW);
  analogWrite (L_motor_speed, L_speed );
  analogWrite (R_motor_speed, R_speed );
  delay (t);
}
void backward (int t) // функция движения назад
{
  digitalWrite (R_motor_F, LOW);
  digitalWrite (L_motor_F, LOW);
  digitalWrite (R_motor_B, HIGH);
  digitalWrite (L_motor_B, HIGH);
  analogWrite (L_motor_speed, L_speed);
  analogWrite (R_motor_speed, R_speed);
  delay (t);
}
void right (int t) //резкий поворот направо
{
  digitalWrite (R_motor_F, LOW);
  digitalWrite (L_motor_F, HIGH);
  digitalWrite (R_motor_B, HIGH);
  digitalWrite (L_motor_B, LOW);
  analogWrite (L_motor_speed, L_speed);
  analogWrite (R_motor_speed, R_speed);
  delay (t);
}
void left (int t) // резкий поворот налево
{
  digitalWrite (R_motor_F, HIGH);
  digitalWrite (L_motor_F, LOW);
  digitalWrite (R_motor_B, LOW);
  digitalWrite (L_motor_B, HIGH);
  analogWrite (L_motor_speed, L_speed);
  analogWrite (R_motor_speed, R_speed);
  delay (t);
}
/*void turn_right (int t) //плавный поворот направо
  {
  digitalWrite (R_motor_F,HIGH);
  digitalWrite (L_motor_F,HIGH);
  digitalWrite (R_motor_B,LOW);
  digitalWrite (L_motor_B,LOW);
  analogWrite (L_motor_speed, 40);
  analogWrite (R_motor_speed, 0);
  delay(t);
  }
  void turn_left (int t) //плавный поворот налево
  {
  digitalWrite (R_motor_F,HIGH);
  digitalWrite (L_motor_F,HIGH);
  digitalWrite (R_motor_B,LOW);
  digitalWrite (L_motor_B,LOW);
  analogWrite (L_motor_speed, 0);
  analogWrite (R_motor_speed, 40);
  delay(t);
  }*/
void Stop (int t)
{
  digitalWrite (R_motor_F, LOW);
  digitalWrite (L_motor_F, LOW );
  digitalWrite (R_motor_B, LOW);
  digitalWrite (L_motor_B, LOW);
  delay (t);
}
void setup()
{
  Serial.begin (9600);
  // моторы
  analogWrite (L_motor_speed, L_speed);
  analogWrite (R_motor_speed, R_speed);
  // датчики
  Sera.attach (3);
  Sera.write (90);
}

void loop()
{
  int L_state = digitalRead(L);
  int R_state = digitalRead(R);

  if (L_state == 1 && R_state == 1) //черная линия посередине
  {
    forward (1);
  }
  else if (L_state == 1 && R_state == 0)
  {
    right (10);
  }
  else if (L_state == 0 && R_state == 1) 
  {
    left (10);
  }
}
