const int LINE_SENSOR_1 = A0;
const int LINE_SENSOR_2 = A1;
const int LINE_SENSOR_3 = A2;
const int LINE_SENSOR_4 = A3;
const int LINE_SENSOR_5 = A4;

const int IN1 = 7;
const int IN2 = 8;
const int IN3 = 11;
const int IN4 = 12;
const int EN1 = 9;
const int EN2 = 10;

void backward (int t) {
  digitalWrite (IN1, HIGH);
  digitalWrite (IN2, LOW);
  analogWrite (EN1, 250); 
  digitalWrite (IN3, HIGH);
  digitalWrite (IN4, LOW);
  analogWrite (EN2, 250); 
  delay (t);
}
void forward (int t) {
  digitalWrite (IN1, LOW);
  digitalWrite (IN2, HIGH);
  analogWrite (EN1, 250); 
  digitalWrite (IN3, LOW);
  digitalWrite (IN4, HIGH);
  analogWrite (EN2, 250); 
  delay (t);
}
void STOP (int t) {
  digitalWrite (IN1, LOW);
  digitalWrite (IN2, LOW);
  analogWrite (EN1, 0); 
  digitalWrite (IN3, LOW);
  digitalWrite (IN4, LOW);
  analogWrite (EN2, 0); 
  delay (t);
}
void right (int t) {
  digitalWrite (IN1, HIGH);
  digitalWrite (IN2, LOW);
  analogWrite (EN1, 250); 
  digitalWrite (IN3, LOW);
  digitalWrite (IN4, HIGH);
  analogWrite (EN2, 250); 
  delay (t);
}
void left (int t) {
  digitalWrite (IN1, LOW);
  digitalWrite (IN2, HIGH);
  analogWrite (EN1, 250); 
  digitalWrite (IN3, HIGH);
  digitalWrite (IN4, LOW);
  analogWrite (EN2, 250); 
  delay (t);
}
void setup() {
  Serial.begin (9600);
  pinMode (LINE_SENSOR_1, INPUT);
  pinMode (LINE_SENSOR_2, INPUT);
  pinMode (LINE_SENSOR_3, INPUT);
  pinMode (LINE_SENSOR_4, INPUT);
  pinMode (LINE_SENSOR_5, INPUT);

  pinMode (IN1, OUTPUT);
  pinMode (IN2, OUTPUT);
  pinMode (IN3, OUTPUT);
  pinMode (IN4, OUTPUT);
  pinMode (EN1, OUTPUT);
  pinMode (EN2, OUTPUT);
}

void loop() {
  forward (1000);
}
