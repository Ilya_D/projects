#include <Servo.h>
Servo servo1;  // create servo object to control a servo
Servo servo2;  // create servo object to control a servo
Servo servo3;  // create servo object to control a servo
const int xdirPin = 5;
const int xstepPin = 2;
const int ydirPin = 6;
const int ystepPin = 3;
const int zdirPin = 7;
const int zstepPin = 4;


const int en1MOT = 44;
const int en2MOT = 46;
int in1 = 31;
int in2 = 29;
int in3 = 27;
int in4 = 25;


bool flag_parking_x = 0;
bool flag_parking_y = 0;
bool flag_parking_z = 0;

int xe_in = 36;
int xe_out = 37;
int ye_in = 32;
int ye_out = 33;
int ze_in = 34;
int ze_out = 35;


void setup()
{
  pinMode(xe_out, OUTPUT);
  pinMode(xe_in, INPUT_PULLUP);
  digitalWrite(xe_out, LOW);
  pinMode(ze_out, OUTPUT);
  pinMode(ze_in, INPUT_PULLUP);
  digitalWrite(ze_out, LOW);
  pinMode(ye_out, OUTPUT);
  pinMode(ye_in, INPUT_PULLUP);
  digitalWrite(ye_out, LOW);
  Serial.begin(9600);

  pinMode(xstepPin, OUTPUT);
  pinMode(xdirPin, OUTPUT);
  pinMode(ystepPin, OUTPUT);
  pinMode(ydirPin, OUTPUT);
  pinMode(zstepPin, OUTPUT);
  pinMode(zdirPin, OUTPUT);


  pinMode(en1MOT, OUTPUT);
  pinMode(en2MOT, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);

  servo1.attach(9);  
  servo1.write(90);               
  servo2.attach(10);  
  servo2.write(85);                  
  servo3.attach(11);  
  servo3.write(80);                  //  145 - 80
  digitalWrite(xdirPin, HIGH);
  while (flag_parking_x == 0)
  {
    bool x_end = digitalRead(xe_in);
    if (x_end == 1)
    {
      Serial.println(x_end);
      digitalWrite(xstepPin, HIGH);
      delayMicroseconds(1000);
      digitalWrite(xstepPin, LOW);
      delayMicroseconds(1000);
    }
    if (x_end == 0)
    {
      digitalWrite(xdirPin, LOW);
      for (int i = 0; i < 500; i++)
      {
        digitalWrite(xstepPin, HIGH);
        delayMicroseconds(1000);
        digitalWrite(xstepPin, LOW);
        delayMicroseconds(1000);
      }
      flag_parking_x = 1;
    }
  }




  digitalWrite(ydirPin, LOW);
  while (flag_parking_y == 0)
  {
    bool y_end = digitalRead(ye_in);
    if (y_end == 1)
    {
      digitalWrite(ystepPin, HIGH);
      delayMicroseconds(5000);
      digitalWrite(ystepPin, LOW);
      delayMicroseconds(5000);
    }
    if (y_end == 0)
    {
      flag_parking_y = 1;
    }
  }







  digitalWrite(zdirPin, LOW);
  while (flag_parking_z == 0)
  {
    bool z_end = digitalRead(ze_in);
    if (z_end == 1)
    {
      digitalWrite(zstepPin, HIGH);
      delayMicroseconds(1000);
      digitalWrite(zstepPin, LOW);
      delayMicroseconds(1000);
    }
    if (z_end == 0)
    {
      digitalWrite(zdirPin, HIGH);
      for (int i = 0; i < 200; i++)
      {
        digitalWrite(zstepPin, HIGH);
        delayMicroseconds(1000);
        digitalWrite(zstepPin, LOW);
        delayMicroseconds(1000);
      }
      flag_parking_z = 1;
    }
  }


}
void loop()
{

}
