#include <SPI.h>          // библиотека для работы с шиной SPI
#include "nRF24L01.h"     // библиотека радиомодуля
#include "RF24.h"         // ещё библиотека радиомодуля

RF24 radio(9, 10); // "создать" модуль на пинах 9 и 10 Для Уно
//RF24 radio(9,53); // для Меги

byte address[][6] = {"1Node", "2Node", "3Node", "4Node", "5Node", "6Node"}; //возможные номера труб





const int T1_1 = 8;
const int T1_2 = 0;
const int T2_1 = A4;
const int T2_2 = A5;
const int T3_1 = 7;
const int T3_2 = 6;
const int T4_1 = 5;
const int T4_2 = 4;
const int T5_1 = 2;
const int T5_2 = 3;
const int P6 = A0;
const int jx = A1;
const int jy = A3;
const int jsw = A2;
int transmit_data[14];
void read_data ()
{
  transmit_data[0] = digitalRead(T1_1);
  transmit_data[1] = digitalRead(T1_2);
  transmit_data[2] = digitalRead(T2_1);
  transmit_data[3] = digitalRead(T2_2);
  transmit_data[4] = digitalRead(T3_1);
  transmit_data[5] = digitalRead(T3_2);
  transmit_data[6] = digitalRead(T4_1);
  transmit_data[7] = digitalRead(T4_2);
  transmit_data[8] = digitalRead(T5_1);
  transmit_data[9] = digitalRead(T5_2);
  transmit_data[10] = analogRead(P6);
  transmit_data[11] = analogRead(jx);
  transmit_data[12] = analogRead(jy);
  transmit_data[13] = digitalRead(jsw);
}
void setup()
{
  radio.begin();              // активировать модуль
  radio.setAutoAck(1);        // режим подтверждения приёма, 1 вкл 0 выкл
  radio.setRetries(0, 15);    // (время между попыткой достучаться, число попыток)
  radio.enableAckPayload();   // разрешить отсылку данных в ответ на входящий сигнал
  radio.setPayloadSize(32);   // размер пакета, в байтах

  radio.openWritingPipe(address[0]);  // мы - труба 0, открываем канал для передачи данных
  radio.setChannel(0x60);             // выбираем канал (в котором нет шумов!)

  radio.setPALevel (RF24_PA_MAX);   // уровень мощности передатчика. На выбор RF24_PA_MIN, RF24_PA_LOW, RF24_PA_HIGH, RF24_PA_MAX
  radio.setDataRate (RF24_250KBPS); // скорость обмена. На выбор RF24_2MBPS, RF24_1MBPS, RF24_250KBPS
  //должна быть одинакова на приёмнике и передатчике!
  //при самой низкой скорости имеем самую высокую чувствительность и дальность!!
  radio.powerUp();
  radio.stopListening();  // не слушаем радиоэфир, мы передатчик




  pinMode(T1_1, INPUT_PULLUP);
  pinMode(T1_2, INPUT_PULLUP);
  pinMode(T2_1, INPUT_PULLUP);
  pinMode(T2_2, INPUT_PULLUP);
  pinMode(T3_1, INPUT_PULLUP);
  pinMode(T3_2, INPUT_PULLUP);
  pinMode(T4_1, INPUT_PULLUP);
  pinMode(T4_2, INPUT_PULLUP);
  pinMode(T5_1, INPUT_PULLUP);
  pinMode(T5_2, INPUT_PULLUP);
  pinMode(P6, INPUT);
  pinMode(jx, INPUT);
  pinMode(jy, INPUT);
  pinMode(jsw, INPUT_PULLUP);
  Serial.begin(9600);
}

void loop()
{
  read_data ();
  radio.write(&transmit_data, sizeof(transmit_data));
}
