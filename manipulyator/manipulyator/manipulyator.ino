#include <Servo.h>

Servo Servo1;
Servo Servo2;
Servo Servo3;
Servo Servo4;
Servo Servo5;
Servo Servo6;

const int pot1 = A0; //превязываем потенциомер к аналоговым ринам
const int pot2 = A1;
const int pot3 = A2;
const int pot4 = A3;
const int pot5 = A4;
const int pot6 = A5;

int potVal1;  //переменная для хранения значения с пот.
int potVal2;
int potVal3;
int potVal4;
int potVal5;
int potVal6;

int angle1;   //переменная для значения угла поворота
int angle2;
int angle3;
int angle4;
int angle5;
int angle6;

void setup() {
  Servo1.attach(3); //подключение сервы
  Servo2.attach(5);
  Servo3.attach(6);
  Servo4.attach(9);
  Servo5.attach(10);
  Servo6.attach(11);
  Serial.begin(9600); // подключаем монитор порта
}

void loop() {
  potVal1 = analogRead(pot1); // чтение потенциометра
  potVal2 = analogRead(pot2);
  potVal3 = analogRead(pot3);
  potVal4 = analogRead(pot4);
  potVal5 = analogRead(pot5);
  potVal6 = analogRead(pot6);

 // перерасчет значения с потенциометра в угол для поворота сервы
  angle1 = map(potVal1, 0, 1023, 180, 0);
  angle2 = map(potVal2, 0, 1023, 0, 180);
  angle3 = map(potVal3, 0, 1023, 0, 180);
  angle4 = map(potVal4, 0, 1023, 0, 180);
  angle5 = map(potVal5, 0, 1023, 0, 180);
  angle6 = map(potVal6, 0, 1023, 0, 180);

  //блок для отладки
  Serial.print ("pot1 = ");
  Serial.print (potVal1);
  Serial.print (", angle = ");
  Serial.println (angle1);

  Serial.print ("pot2 = ");
  Serial.print (potVal2);
  Serial.print (", angle2 = ");
  Serial.println(angle2);

  Serial.print ("pot3 = ");
  Serial.print (potVal3);
  Serial.print (", angle3: ");
  Serial.println(angle3);

  Serial.print("pot4 = ");
  Serial.print(potVal4);
  Serial.print(", angle4: ");
  Serial.println(angle4);

  Serial.print("pot5 = ");
  Serial.print(potVal5);
  Serial.print(", angle5: ");
  Serial.println(angle5);

  Serial.print("pot6 = ");
  Serial.print(potVal6);
  Serial.print(", angle6: ");
  Serial.println(angle6);
  Serial.println("");

  Servo1.write(angle1);// поворот сервы на заданный угол
  Servo2.write(angle2);
  Servo3.write(angle3);
  Servo4.write(angle4);
  Servo5.write(angle5);
  Servo6.write(angle6);
}
