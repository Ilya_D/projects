#include <NewPing.h>

const int TRIG1 = 12;
const int ECHO1 = 13;
const int TRIG3 = 2;
const int ECHO3 = 4;
const int TRIG4 = 22;
const int ECHO4 = 21;
const int MAX_DISTANCE = 50;
const int TRIG2 = 7;
const int ECHO2 = 8;
// 7ми-сегментный индикатор
const int A = 20;
const int B = 19;
const int C = 18;
const int D = 17;
const int E = 16;
const int F = 15;
const int G = 14;

NewPing sonar1_people (TRIG1, ECHO1, MAX_DISTANCE);
NewPing sonar2_people (TRIG3, ECHO3, MAX_DISTANCE);

NewPing sonar1_car (TRIG2, ECHO2, MAX_DISTANCE);
NewPing sonar2_car (TRIG4, ECHO4, MAX_DISTANCE);


const int red2 = 9;
const int green2 = 10;
const int blue2 = 11;
const int red = 3;
const int green = 5;
const int blue = 6;

void zero (int t)
{
  digitalWrite(A, HIGH); //цифра нуль
  digitalWrite(B, HIGH);
  digitalWrite(C, HIGH);
  digitalWrite(D, HIGH);
  digitalWrite(E, HIGH);
  digitalWrite(F, HIGH);
  digitalWrite(G, LOW);
  delay(t);
}
void one (int t)
{
  digitalWrite(A, LOW); //цифра один
  digitalWrite(B, HIGH);
  digitalWrite(C, HIGH);
  digitalWrite(D, LOW);
  digitalWrite(E, LOW);
  digitalWrite(F, LOW);
  digitalWrite(G, LOW);
  delay(t);
}

void yellow (int t)
{
  analogWrite (red, 220);
  analogWrite (green, 250);
  delay(t);
}
void yellow2 (int t)
{
  analogWrite (red2, 220);
  analogWrite (green2, 250);
  delay(t);
}
void yellow_off (int t)
{
  digitalWrite (red, LOW);
  digitalWrite (green, LOW);
  delay(t);
}
void yellow2_off (int t)
{
  digitalWrite (red2, LOW);
  digitalWrite (green2, LOW);
  delay(t);
}
void people_green (int t)
{
  one (1000);
  zero (1000);
  /*yellow (0);
  yellow2 (0);
  delay(1000);
  yellow_off (0);
  yellow2_off (0);
  delay (100);*/
  digitalWrite (red2, HIGH);// красный машинам
  delay (2000);
  digitalWrite (green, HIGH); // зеленый людям
  delay (t);
  //выключаем все
  digitalWrite (green, LOW);
  digitalWrite (red, LOW);
  digitalWrite (red2, LOW);
  digitalWrite (green2, LOW);

}
void people_red (int t)
{
  one (1000);
  zero (1000);
  /*yellow (0);
  yellow2 (0);
  delay(1000);
  yellow_off (0);
  yellow2_off (0);*/
  digitalWrite (red, HIGH); // красный людям
  delay (2000);
  digitalWrite (green2, HIGH); // зеленый машинам
  delay (t);
  digitalWrite (green, LOW);
  digitalWrite (red, LOW);
  digitalWrite (red2, LOW);
  digitalWrite (green2, LOW);
}

void setup()
{
  Serial.begin(9600);
  pinMode (red, OUTPUT);
  pinMode (green, OUTPUT);
  pinMode (blue, OUTPUT);
  pinMode (red2, OUTPUT);
  pinMode (green2, OUTPUT);
  pinMode (blue2, OUTPUT);
}
void loop()
{
  delay(50);
  
  unsigned int distance_people1 = sonar1_people.ping_cm();
  unsigned int distance_people2 = sonar2_people.ping_cm();

  unsigned int distance_car1 = sonar1_car.ping_cm();
  unsigned int distance_car2 = sonar2_car.ping_cm();

  if (distance_people1 < 30 && distance_people1 > 0)
  {
    people_green (10000);
  }
  Serial.print ("сонар человеки = ");
  Serial.print(distance_people1);
  Serial.println("см");
  if (distance_car < 30 && distance_car > 0)
  {
    people_red (6000);
  }
  Serial.print ("сонар машины = ");
  Serial.print(distance_car);
  Serial.println("см");
  delay(300);

}
